# Do Krylov time evolution with random matrix
# There are a few different packages that should work, namely KrylovKit and ExpoKit.
# I show how to use each (commented), but by default use my own modification of exmpv from expokit, which allows pre-allocation of matrices to save space.

# The most important thing for any of these is to specify the number of threads to use when on a cluster. For instance, if you want each julia script to operate
# on a single core, add the following to your shell script (removing leading #):
#
# export JULIA_NUM_THREADS=1
# export OPENBLAS_NUM_THREADS=1
# export OMP_NUM_THREADS=1
# julia test_krylov_kit.jl # Will run code with 1 thread per job, avoids crashing the cluster. You may be able to parallelize by increasing this number.


using KrylovKit
using Expokit
using SparseArrays
using Random
using LinearAlgebra
include("mk_expmv.jl")

println("Here")

Random.seed!(0)

basis_sz=20 # Size of Hilbert space
n_sparse_vals=40 # Number of random non-zero elements
t=1.0

H=spzeros(ComplexF64,basis_sz,basis_sz)
psi=rand(basis_sz)+im*rand(basis_sz)
psi=psi/norm(psi)

# Construct a rand sparse Hamiltonian
for j in 1:n_sparse_vals
    H[rand(1:basis_sz),rand(1:basis_sz)]=rand()+im*rand()
end

H=H+H'

# Dense matrix exponentiate
println("Dense")
psif_slow=exp(-im*t*Matrix(H))*psi

# KrylovKit
println("KrylovKit")
psif_krylovkit,info=exponentiate(H,-im*t,psi,ishermitian=true)
@assert(info.converged==1)

# Expokit
println("Expokit")
psif_expokit=expmv(-im*t,H,psi)

# mk_expmv.jl
println("mk_expmv")
# Pre-allocate Krylov objects
mm=min(30,size(H,1)) # Krylov size
vwork = Array{typeof(psi)}(undef,mm+1) # Krylov vectors
for i=1:mm+1
    vwork[i]=similar(psi)
end
hwork=zeros(ComplexF64,mm+2,mm+2) # Krylov Hamiltonian
pwork=similar(psi) # Working space for wfn
Fwork=similar(hwork) # Working space for small matrix exponential

psif_mk=psi # Alias, doesn't copy vector
expmv!(-im*t,H,psif_mk,vwork,hwork,pwork,Fwork) # Overwrites psif_mk

println("norm(psif_slow-psif_krylovkit)=",norm(psif_slow-psif_krylovkit))
println("norm(psif_slow-psif_expokit)=",norm(psif_slow-psif_expokit))
println("norm(psif_slow-psif_mk)=",norm(psif_slow-psif_mk))
